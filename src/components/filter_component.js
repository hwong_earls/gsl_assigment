'use strict';

import React from 'react';
import { callApi, QUEUE } from '../commons/library.js';
import { CITIES } from '../fixed_data/cities_data.js';


export class Filter extends React.Component
{
    constructor(props) {
        super(props);

        this.state = { 'city': '' };
        this.changeCity = this.changeCityAction.bind(this);
        this.searchAction = this.searchAction.bind(this);
    }

    searchAction(event) {

        var cityQuery = this.state.city;
        var apiUrl = 'http://api.openweathermap.org/data/2.5/weather?units=metric&q='+cityQuery;
        callApi('GET', null, apiUrl, function(data) {
            QUEUE.publish({ 'queue': 'filter_current', 'data': data });
        } );
        var apiUrl = 'http://api.openweathermap.org/data/2.5/forecast/daily?cnt=7&units=metric&q='+cityQuery;
        callApi('GET', null, apiUrl, function(data) {
            QUEUE.publish({ 'queue': 'filter_forecast', 'data': data });
        } );
        event.preventDefault();
    }

    changeCityAction(event) {
        this.setState({ 'city': event.target.value });
    }

    render() {
        
        var optionCities = [];
        var count = 0;
        optionCities.push(<option disabled value='' key='city_default'> -- select an option -- </option>);
        for(var city of CITIES){
            var key = 'city_' + (++count);
            optionCities.push(<option key={key}>{city}</option>);
        }

        return (
            <div className='container'>
                <form onSubmit={this.searchAction}>
                	<select className='selectpicker' data-live-search='true' name='field_city' onChange={this.changeCity} defaultValue=''>
                		{optionCities}
                	</select>
                	<button type='submit' className='btn btn-primary' disabled={!this.state.city}>Get Weather</button>
                </form>
            </div>
        );
    }
}
