'use strict';

import React from 'react';
import { QUEUE, parseDate } from '../commons/library.js';


export class Content_Current extends React.Component
{
    constructor(props) {
        super(props);
        this.state = { 'city': null };
        const $this = this;            
        QUEUE.addSubscribe({ 'queue': 'filter_current', 'subscriber': function(data) {
            $this.changeData($this, data);
        } });        
    }

    changeData($this, data) {
        $this.setState({ 'city': data });
    }

    render() {

        if(this.state.city == null){

            return (
                    <span>No city selected, please select one city.</span>
            );
        }
                
        return (
            <div className='container current_section'>
                <div className='row'>
                	<div className='col-sm-12'><span className='title_current'>Current Condition:</span></div>
                </div>
                <div className='row'>
                	<div className='col-sm-4'>City:</div>
                	<div className='col-sm-8'>{this.state.city.name}, {this.state.city.sys.country}</div>
                </div>
                <div className='row'>
                	<div className='col-sm-4'>Condition:</div>
                	<div className='col-sm-8'>{this.state.city.weather[0].main}</div>
                </div>
                <div className='row'>
                	<div className='col-sm-4'>Description:</div>
                	<div className='col-sm-8'>{this.state.city.weather[0].description}</div>
                </div>
                <div className='row'>
                	<div className='col-sm-4'>Temperature:</div>
                	<div className='col-sm-8'>{this.state.city.main.temp}&nbsp;&deg;C</div>
                </div>
                <div className='row'>
                	<div className='col-sm-4'>Min. Temperature:</div>
                	<div className='col-sm-8'>{this.state.city.main.temp_min}&nbsp;&deg;C</div>
                </div>
                <div className='row'>
                	<div className='col-sm-4'>Max. Temperature:</div>
                	<div className='col-sm-8'>{this.state.city.main.temp_max}&nbsp;&deg;C</div>
                </div>
                <div className='row'>
                	<div className='col-sm-4'>Humidity:</div>
                	<div className='col-sm-8'>{this.state.city.main.humidity}&nbsp;%</div>
                </div>
                <div className='row'>
                	<div className='col-sm-4'>Wind Speed:</div>
                	<div className='col-sm-8'>{this.state.city.wind.speed}&nbsp;Km/H</div>
                </div>
            </div>
        );
    }
}

export class Content_Forecast extends React.Component
{
    constructor(props) {
        super(props);
        this.state = { 'forecast': null };
        const $this = this;
        QUEUE.addSubscribe({ 'queue': 'filter_forecast', 'subscriber': function(data) {
            $this.changeData($this, data);
        } });
    }

    changeData($this, data) {
        $this.setState({ 'forecast': data });
    }

    render() {
        
        if(this.state.forecast == null){

            return (
            	<div className='container'>
                </div>
            );
        }

        var forecast_days = [];
        var countDays = 1;
        forecast_days.push(
        	<div className='col-sm-2' key='forecast-headers'>
            	<div className='row'>
                	<div className='col-sm-12'><span>Day:</span></div>
                </div>
            	<div className='row'>
                	<div className='col-sm-12'><span>&nbsp;</span></div>
                </div>
                <div className='row'>
                  	<div className='col-sm-12'><span>Condition:</span></div>
                </div>
                <div className='row'>
                   	<div className='col-sm-12'><span>Temperature:</span></div>
                </div>
                <div className='row'>
                   	<div className='col-sm-12'><span>Min. Temp.:</span></div>
                </div>
                <div className='row'>
                   	<div className='col-sm-12'><span>Max. Temp.:</span></div>
                </div>
            </div>
        );
        for(var days of this.state.forecast.list){
            var key = 'forecast-' + countDays;
            var date = parseDate(days.dt);
            forecast_days.push(
                <div className='col-sm-1' key={key}>
                    <div className='row'>
                    	<div className='col-sm-12'><span>{date.weekday}</span></div>
                    </div>
                    <div className='row'>
                    	<div className='col-sm-12'><span>{date.dateFormatted}</span></div>
                    </div>
                    <div className='row'>
                    	<div className='col-sm-12'><span>{days.weather[0].main}</span></div>
                    </div>
                    <div className='row'>
                    	<div className='col-sm-12'><span>{days.temp.day}&nbsp;&deg;C</span></div>
                    </div>
                    <div className='row'>
                    	<div className='col-sm-12'><span>{days.temp.min}&nbsp;&deg;C</span></div>
                    </div>
                    <div className='row'>
                    	<div className='col-sm-12'><span>{days.temp.max}&nbsp;&deg;C</span></div>
                    </div>
                </div>
            );
            countDays++;
        }

        return (
            <div className='container forecast_section'>
                <span className='title_forecast'>Forecast:</span>
                <div className='row'>
                	{forecast_days}
                </div>
            </div>
        );
    }
}
