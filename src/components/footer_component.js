'use strict';

import React from 'react';


export class Footer extends React.Component
{
    render() {
        
        const styleFooter = {
            'position': 'absolute',
            'bottom': '0'
        };

        return (
                <footer className="footer footer_default" style={ styleFooter }>
                	<p>Javascript ReactJS application with APi example - Author: Henry Wong (2017)</p>
                </footer>
        );
    }
}

