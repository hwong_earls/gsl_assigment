/*
 *	Application: Homepage
 *
 *	Assigment for GSL Holding
 *	This is a small Javascript front-end application using ReactJS, Bootstrap, HTML, CSS.
 *	Author: Henry Wong
 *	Date: April 30, 2017
 */

import React from 'react';
import ReactDOM from 'react-dom';

import { Main_Template } from './templates/main_template.js';
import { QUEUE } from './commons/library.js'; // Invoke QUEUE in order to initialise it before start to add subscribers


ReactDOM.render(
	<div className='container'>
		<Main_Template />
    </div>
,
document.body.appendChild(document.createElement('div')));
