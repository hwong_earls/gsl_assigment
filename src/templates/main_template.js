'use strict';

import React from 'react';

import { Footer } from '../components/footer_component.js';
import { Filter } from '../components/filter_component.js';
import { Content_Current, Content_Forecast } from '../components/content_component.js';


export class Main_Template extends React.Component
{
    render() {
        
        return (
    		<div>
           	    <Filter />
                <main>
            		<Content_Current />
                    <Content_Forecast />
                </main>
                <Footer />
            </div>
        );
    }
}
