FROM nginx
MAINTAINER Henry Wong <henrywong72@hotmail.com>
COPY js /usr/share/nginx/html/js
COPY index.html /usr/share/nginx/html
COPY styles /usr/share/nginx/html/styles
COPY vendor /usr/share/nginx/html/vendor
