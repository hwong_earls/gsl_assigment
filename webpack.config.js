const webpack = require('webpack');

var path = process.cwd();

module.exports = function(env){

    return [
        {
            entry: path+'/src/app.js',
            output: {
                path: path+'/js',
                filename: 'bundle.js'
            },
            module: {
                loaders: [
                    {
                        test: /\.jsx?$/,
                        exclude: /node_modules/,
                        loader: 'babel-loader',
                    },
                    {
                        test: /\.css$/,
                        loader: 'style!css'
                    }
                ]
            }
        }
    ];
}
